// { Driver Code Starts
#include<bits/stdc++.h>
using namespace std;

int max_sum(int A[],int N);

int main()
{
    int T;
    cin>>T;
    while(T--)
    {
        int N;
        cin>>N;
        int A[N];
        for(int i=0;i<N;i++)
        {
            cin>>A[i];
        }

        cout<<max_sum(A,N)<<endl;
        /*keeping track of the total sum of the array*/

    }
}
// } Driver Code Ends


/*You are required to complete this method*/

int max_sum(int A[],int N)
{
    int add_all=0;
for(int i=0;i<N;i++){
    int temp_sum=0; 
    for(int j=0;j<N;j++){
        
        int formula=(N-j)-(N-i);
        int index = formula >=N ? formula-N:formula;
        cout<< index;
        temp_sum += A[index]*j;
    }
    add_all= add_all< temp_sum?temp_sum:add_all;
}
return add_all;
}
