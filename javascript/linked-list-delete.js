const SinglyLinkedListNode = class {
    constructor(nodeData) {
        this.data = nodeData;
        this.next = null;
    }
};

const SinglyLinkedList = class {
    constructor() {
        this.head = null;
        this.tail = null;
    }

    insertNode(nodeData) {
        const node = new SinglyLinkedListNode(nodeData);

        if (this.head == null) {
            this.head = node;
        } else {
            this.tail.next = node;
        }

        this.tail = node;
    }
};

function printSinglyLinkedList(node) {
    while (node != null) {
        console.log(node.data)
        node = node.next;
    }
}

// Complete the insertNodeAtTail function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
function insertNodeAtPosition(llist, data, position) {
    let node = new SinglyLinkedListNode(data);
    if (position == 0) {
        node.next = llist;
        llist = node;
        return llist;
    } else {
        var current, prev;
        current = llist;
        var increment = 0;
        while (increment < position) {
            increment++;
            prev = current;
            current = current.next;
        }
        node.next = current;
        prev.next = node;
        return llist;
    }
}

function deleteNode(llist, position) {
    // Write your code here
    if (position == 0) {
        return llist.next
    } else {
        var current, prev;
        current = llist;
        var increment = 0;
        while (increment < position) {
            increment++;
            prev = current;
            current = current.next;
        }
        prev.next = current.next;
        return llist;
    }
}



function main() {
    list_item = [345, 56, 23, 56, 23];
    let llist = new SinglyLinkedList();

    for (let i = 0; i < list_item.length; i++) {
        llist.insertNode(list_item[i]);
    }

    let llist_head = deleteNode(llist.head, 2);

    printSinglyLinkedList(llist_head)
}
main();