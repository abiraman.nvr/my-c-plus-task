const SinglyLinkedListNode = class {
    constructor(nodeData) {
        this.data = nodeData;
        this.next = null;
    }
};

const SinglyLinkedList = class {
    constructor() {
        this.head = null;
        this.tail = null;
    }

    insertNode(nodeData) {
        const node = new SinglyLinkedListNode(nodeData);

        if (this.head == null) {
            this.head = node;
        } else {
            this.tail.next = node;
        }

        this.tail = node;
    }
};

function printSinglyLinkedList(node) {
    while (node != null) {
        console.log(node.data)
        node = node.next;
    }
}

// Complete the CompareLists function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
function CompareLists(llist1, llist2) {
    var ll_length = 0;
    var ll2_length = 0;
    while (llist1 != null && llist2 != null) {
        if (llist2.data !== llist1.data) {
            return false;
        }
        if (llist2.next !== null) { ll2_length++; }
        if (llist1.next !== null) { ll_length++; }
        llist2 = llist2.next;
        llist1 = llist1.next;

    }
    if (ll2_length != ll_length) return false;
    return true;
}

function main() {


    const llist1Item = [23, 45, 12, 5465, 56]

    let llist1 = new SinglyLinkedList();

    for (let i = 0; i < llist1Item.length; i++) {
        llist1.insertNode(llist1Item[i]);
    }

    const llist2Item = [23, 45, 12]

    let llist2 = new SinglyLinkedList();

    for (let i = 0; i < llist2Item.length; i++) {
        llist2.insertNode(llist2Item[i]);
    }

    let result = CompareLists(llist1.head, llist2.head);

    console.log((result ? 1 : 0) + "\n");


}
main();