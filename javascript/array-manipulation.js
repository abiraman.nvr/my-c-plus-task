function arrayManipulation(n, queries) {
    var array = new Array(n).fill(0);
    for (let i = 0; i < queries.length; i++) {
        array[queries[i][0] - 1] += queries[i][2];
        array[queries[i][1]] -= queries[i][2];
    }
    var sum = 0;
    var max = 0;
    for (let i = 0; i < array.length; i++) {
        sum += array[i];
        max = Math.max(sum, max)

    }
    return max

}
var n = 10
var queries = [
    [2, 3, 603],
    [1, 1, 286],
    [4, 4, 882],
]

console.log(arrayManipulation(n, queries))