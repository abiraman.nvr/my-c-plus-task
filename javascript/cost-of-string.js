var input = [
    [-1, -1, 0, -9, -2, -2],
    [-2, -1, -6, -8, -2, -5],
    [-1, -1, -1, -2, -3, -4],
    [-1, -9, -2, -4, -4, -5],
    [-7, -3, -3, -2, -9, -9],
    [-1, -3, -1, -2, -4, -5],
]



function matrix(array, start, end) {
    var temp = [];
    var sum = 0
    for (let i = start; i < 3 + start; i++) {
        for (let j = end; j < 3 + end; j++) {
            temp.push(array[i][j]);
        }
    }
    return temp;
}


function hourglassSum(input) {
    var result = [];
    var length = input[0].length;
    for (let k = 0; k < length - 2; k++) {
        for (let n = 0; n < length - 2; n++) {
            result.push(matrix(input, k, n));
        }
    }
    return sum(result);
}

function sum(result) {
    var graterthan = Number.NEGATIVE_INFINITY;
    for (let k = 0; k < result.length; k++) {
        var sum = 0;
        for (let n = 0; n < result[k].length; n++) {
            if (n == 3 || n == 5) continue;
            sum += result[k][n];
        }
        if (graterthan < sum) {
            graterthan = sum
            console.log(graterthan)
        }
    }

    return graterthan;
}

console.log(hourglassSum(input))