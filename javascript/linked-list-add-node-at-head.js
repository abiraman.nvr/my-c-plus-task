const SinglyLinkedListNode = class {
    constructor(nodeData) {
        this.data = nodeData;
        this.next = null;
    }
};

const SinglyLinkedList = class {
    constructor() {
        this.head = null;
        this.tail = null;
    }

};

function printSinglyLinkedList(node) {
    while (node != null) {
        console.log(node.data)
        node = node.next;
    }
}

// Complete the insertNodeAtTail function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
function insertNodeAtHead(head, data) {
    let node = new SinglyLinkedListNode(data);

    var current = head;
    node.next = current;
    head = node;
    return head;
}

function main() {
    list_item = [345, 56, 23, 56, 23];
    let llist = new SinglyLinkedList();

    for (let i = 0; i < list_item.length; i++) {
        const llist_head = insertNodeAtHead(llist.head, list_item[i]);
        llist.head = llist_head;
    }
    printSinglyLinkedList(llist.head);
}
main();