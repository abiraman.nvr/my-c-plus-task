const SinglyLinkedListNode = class {
    constructor(nodeData) {
        this.data = nodeData;
        this.next = null;
    }
};

const SinglyLinkedList = class {
    constructor() {
        this.head = null;
        this.tail = null;
    }

    insertNode(nodeData) {
        const node = new SinglyLinkedListNode(nodeData);

        if (this.head == null) {
            this.head = node;
        } else {
            this.tail.next = node;
        }

        this.tail = node;
    }
};

function printSinglyLinkedList(node) {
    while (node != null) {
        console.log(node.data)
        node = node.next;
    }
}

// Complete the insertNodeAtTail function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
function insertNodeAtPosition(llist, data, position) {
    let node = new SinglyLinkedListNode(data);
    if (position == 0) {
        node.next = llist;
        llist = node;
        return llist;
    } else {
        var current, prev;
        current = llist;
        var increment = 0;
        while (increment < position) {
            increment++;
            prev = current;
            current = current.next;
        }
        node.next = current;
        prev.next = node;
        return llist;
    }
}

function reversePrint(llist) {
    var prev = null;
    var next = null;
    var current = llist;
    while (current != null) {
        next = current.next;
        current.next = prev;
        prev = current;
        current = next;
    }
    llist = prev;
    printSinglyLinkedList(llist)
}



function main() {
    list_item = [
        [345, 56, 23, 56, 23],
        [45, 67, 23],
        [12, 34, 1]
    ];

    const tests = 3

    for (let testsItr = 0; testsItr < tests; testsItr++) {

        let llist = new SinglyLinkedList();

        for (let i = 0; i < list_item[testsItr].length; i++) {
            const llistItem = list_item[testsItr][i];
            llist.insertNode(llistItem);
        }

        reversePrint(llist.head);
    }
}
main();