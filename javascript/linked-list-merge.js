const SinglyLinkedListNode = class {
    constructor(nodeData) {
        this.data = nodeData;
        this.next = null;
    }
};

const SinglyLinkedList = class {
    constructor() {
        this.head = null;
        this.tail = null;
    }

    insertNode(nodeData) {
        const node = new SinglyLinkedListNode(nodeData);
        if (this.head == null) {
            this.head = node;
        } else {
            this.tail.next = node;
        }
        this.tail = node;
    }
};

function printSinglyLinkedList(node) {
    while (node != null) {
        console.log(node.data)
        node = node.next;
    }
}

// Complete the CompareLists function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
function mergeLists(llist1, llist2) {
    var newLinked = new SinglyLinkedList()
    while (llist1 != null) {

        while (llist2 != null) {
            if (llist1 == null) {
                newLinked.insertNode(llist2.data);
                llist2 = llist2.next;
            } else if (llist1.data < llist2.data) {
                newLinked.insertNode(llist1.data);
                console.log(llist1.next)
                llist1 = llist1.next;
            } else if (llist1.data == llist2.data) {
                newLinked.insertNode(llist2.data);
                newLinked.insertNode(llist1.data);
                llist2 = llist2.next;
                llist1 = llist1.next;
            } else {
                newLinked.insertNode(llist2.data);
                console.log(llist1.next)
                llist2 = llist2.next;
            }

        }
        if (llist2 == null && llist1 != null) {
            newLinked.insertNode(llist1.data);
            llist1 = llist1.next;
        }

    }
    return newLinked;
}

function main() {
    const llist1Item = [1, 2, 3]
    let llist1 = new SinglyLinkedList();
    for (let i = 0; i < llist1Item.length; i++) {
        llist1.insertNode(llist1Item[i]);
    }
    const llist2Item = [3, 4];
    let llist2 = new SinglyLinkedList();
    for (let i = 0; i < llist2Item.length; i++) {
        llist2.insertNode(llist2Item[i]);
    }
    let llist3 = mergeLists(llist1.head, llist2.head);
    printSinglyLinkedList(llist3.head);
}
main();