const SinglyLinkedListNode = class {
    constructor(nodeData) {
        this.data = nodeData;
        this.next = null;
    }
};

const SinglyLinkedList = class {
    constructor() {
        this.head = null;
        this.size = 0;
    }

};

function printSinglyLinkedList(node) {
    while (node != null) {
        console.log(node.data)
        node = node.next;
    }
}

// Complete the insertNodeAtTail function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
function insertNodeAtTail(head, data) {
    let node = new SinglyLinkedListNode(data);

    var current;
    if (head == null) {
        head = node;
        return head;
    } else {
        current = head;
        while (current.next) {
            current = current.next;
        }
        current.next = node;

    }
    return head;
}

function main() {
    list_item = [345, 56, 23, 56, 23];
    let llist = new SinglyLinkedList();

    for (let i = 0; i < list_item.length; i++) {
        const llist_head = insertNodeAtTail(llist.head, list_item[i]);
        llist.head = llist_head;
    }
    printSinglyLinkedList(llist.head);
}
main();