class treeNode {
    constructor(data) {
        this.data = data;
        this.right = null;
        this.left = null
    }
}
class binaryTree {
    constructor() {
        this.root = null
        this.display = []
    }
    insertData(data) {
        let node = new treeNode(data);
        if (this.root == null) {
            this.root = node
        } else {
            this.insertInTree(this.root, node)
        }
    }
    insertInTree(parent, child) {
        if (child.data > parent.data) {
            if (parent.right == null) {
                parent.right = child
            } else {
                this.insertInTree(parent.right, child)
            }
        } else {
            if (parent.left == null) {
                parent.left = child
            } else {
                this.insertInTree(parent.left, child)
            }
        }
    }
    inorder(root) {
        const nodes = []
        if (root !== null) {
            console.log(root.data)
            this.inorder(root.left)
                // this.display.push(root.data)
            this.inorder(root.right)
        }

    }
    deteleBinaryNode(root, value) {
        if (root == null) return null;
        else if (value < root.data) {
            root.left = this.deteleBinaryNode(root.left, value);
            return root;
        } else if (value > root.data) {
            root.right = this.deteleBinaryNode(root.right, value);
            return root;

        } else {
            if (root.left == null && root.right == null) {
                root = null;
                return root;
            }
            // Single Child cases
            if (root.left === null) return root.right;
            if (root.right === null) return root.left;
            let currtNode = root.right;
            while (currtNode.left != null) {
                currtNode = currtNode.left;
            }
            root.data = currtNode.data;
            root.right = this.deteleBinaryNode(root.right, root.data)
            return root;
        }
    }
}
let bTree = new binaryTree();
let array = [8, 3, 1, 6, 4, 7, 10, 14, 9, 13];
for (let i = 0; i < array.length; i++) {
    bTree.insertData(array[i])
}
bTree.inorder(bTree.root)
bTree.deteleBinaryNode(bTree.root, 3);
bTree.inorder(bTree.root)